const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
require("dotenv").config();

const app = express();

app.use(cors());

// bodyParser = {
//     json: { limit: "50mb", extended: true },
//     urlencoded: { limit: "50mb", extended: true },
// };
// app.use(bodyParser.json());
app.use(bodyParser.json({ limit: "50mb", extended: true }));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

// --------------------------------------------------
const objectRouters = require("./routes/objects");
app.use("/object", objectRouters);

const cityRouters = require("./routes/cities");
app.use("/city", cityRouters);

// console.log(process.env.MONGO_URI, 2);

mongoose
    .connect(process.env.MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: true,
        useCreateIndex: true,
    })
    .then(() => {
        console.log("connected to db...");
    })
    .catch((err) => {
        console.log("DB connection error", err);
    });

app.listen(process.env.PORT || 3031, function () {
    console.log(
        "Express server listening on port %d in %s mode",
        this.address().port,
        app.settings.env
    );
});
