const express = require("express");
const router = express.Router();
const City = require("../models/city");

// **********************************************************************************************

router.get("/", async (req, res) => {
    try {
        const cities = await City.find();
        res.json({
            message: "list of cities loaded successfully",
            result: cities,
        });
    } catch (err) {
        res.json({ message: err });
    }
});
// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************
router.post("/add", async (req, res) => {
    try {
        console.log("cities/add");
        const { city, country, long, lat } = req.body;
        const newCity = new City({
            city,
            country,
            long,
            lat,
        });
        await newCity.save();
        return res.status(200).json({
            message: "City added successfully",
            result: newCity,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});
// **********************************************************************************************
// router.post("/edit", async (req, res) => {
//     try {
//         const { id, field, value } = req.body;
//         const object = await Object.findOne({ _id: id });
//         // console.log(osbject);
//         if (!object) {
//             throw "Object with such id is not found";
//         }
//         if (!object[field]) {
//             throw "There is no such field in the file";
//         }
//         object[field] = value;
//         await object.save();

//         return res.status(200).json({
//             message: "object updated successfully",
//             result: object,
//         });
//     } catch (error) {
//         const code = Math.random() * 100000;
//         console.log(error, code);
//         res.status(500).json({
//             message: "Internal server error",
//             codeoferror: code,
//         });
//     }
// });

// **********************************************************************************************

module.exports = router;
