const express = require("express");
const router = express.Router();
const Object = require("../models/objects");
const City = require("../models/city");

// **********************************************************************************************

router.get("/", async (req, res) => {
    try {
        const objects = await Object.find();
        res.json({
            message: "list of objects loaded successfully",
            result: objects,
        });
    } catch (err) {
        res.json({ message: err });
    }
});
// **********************************************************************************************
// **********************************************************************************************
// **********************************************************************************************
router.post("/new", async (req, res) => {
    try {
        const { name, address, city, country, long, lat, images } = req.body;
        const newObject = new Object({
            name,
            address,
            city,
            country,
            long: long,
            lat: lat,

            minPrice: 0,
            activeItemCount: 0,
            likes: 0,

            nameDrafted: name,
            addressDrafted: address,
            cityDrafted: city,
            countryDrafted: country,
            longDrafted: long,
            latDrafted: lat,
            imagesDrafted: [...images],
        });
        await newObject.save();
        return res.status(200).json({
            message: "object added successfully",
            result: newObject,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});
// **********************************************************************************************
router.post("/newfromsheet", async (req, res) => {
    try {
        const {
            name,
            address,
            cityId,
            long,
            lat,
            images,
            zip,
            cityRegion,
            mapSaved,
        } = req.body;

        console.log(
            name,
            address,
            cityId,
            long,
            lat,
            images,
            zip,
            cityRegion,
            mapSaved
        );
        if (!cityId) throw "city id must be in request";
        const _city = await City.findOne({ _id: cityId });
        if (!_city) throw "city with this id is not found";
        const { city, country } = _city;

        console.log(_city, city, country);

        const maps = {
            mapSaved2gis: "",
            mapSavedGoogle: "",
            mapSavedYandex: "",
        };
        if (mapSaved) {
            if (mapSaved.includes("2gis")) {
                maps.mapSaved2gis = mapSaved;
            }
            if (mapSaved.includes("google")) {
                maps.mapSavedGoogle = mapSaved;
            }
            if (mapSaved.includes("yandex")) {
                maps.mapSavedYandex = mapSaved;
            }
        }

        const newObject = new Object({
            ...maps,
            name,
            address,

            cityRegion,
            zip,

            city,
            country,
            long: long,
            lat: lat,

            minPrice: 0,
            activeItemCount: 0,
            likes: 0,

            nameDrafted: name,
            addressDrafted: address,
            cityRegionDrafted: cityRegion,
            cityDrafted: city,
            countryDrafted: country,
            longDrafted: long,
            latDrafted: lat,
            imagesDrafted: [...images],
        });
        await newObject.save();
        return res.status(200).json({
            message: "object added successfully",
            result: newObject,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});

// **********************************************************************************************
router.post("/edit", async (req, res) => {
    try {
        const { id, field, value } = req.body;
        const object = await Object.findOne({ _id: id });
        // console.log(osbject);
        if (!object) {
            throw "Object with such id is not found";
        }
        if (!object[field]) {
            throw "There is no such field in the file";
        }
        object[field] = value;
        await object.save();

        return res.status(200).json({
            message: "object updated successfully",
            result: object,
        });
    } catch (error) {
        const code = Math.random() * 100000;
        console.log(error, code);
        res.status(500).json({
            message: "Internal server error",
            codeoferror: code,
        });
    }
});

// **********************************************************************************************

module.exports = router;
