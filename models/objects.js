const mongoose = require("mongoose");
const { Schema } = mongoose;

const ObjectScema = Schema({
    name: { type: String, required: true },
    address: { type: String, required: true },
    cityRegion: { type: String },
    zip: { type: String },


    mapSaved2gis: { type: String },
    mapSavedGoogle: { type: String },
    mapSavedYandex: { type: String },

    long: { type: Number, required: true },
    lat: { type: Number, required: true },

    city: { type: String, required: true },
    country: { type: String, required: true },

    isActive: { type: Boolean, default: false },
    isModerated: { type: Boolean, default: false },
    minPrice: Number,
    activeItemCount: Number,
    items: [
        {
            _id: mongoose.Types.ObjectId,
        },
    ],
    images: [
        {
            image: { type: String },
            type: { type: String, defalult: "photo" },
        },
    ],
    likes: Number,
    nameDrafted: { type: String, required: true },
    addressDrafted: { type: String, required: true },
    cityRegionDrafted: { type: String },
    cityDrafted: { type: String, required: true },
    countryDrafted: { type: String, required: true },
    longDrafted: { type: Number, required: true },
    latDrafted: { type: Number, required: true },
    imagesDrafted: [
        {
            image: { type: String },
            type: { type: String, defalult: "photo" },
        },
    ],
});

module.exports = mongoose.model("object", ObjectScema);
